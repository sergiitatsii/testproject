import UIKit
import Model
import FileExhibitsLoader

class ExhibitsViewController: ConfigurableViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var noContentView: UILabel!
  
  var exhibits = [Exhibit]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarVisible
    navigationItem.title = "Exhibits".localized
    configureTableView()
    fetchData()
  }
  
  // MARK: - Private Functions
  private func fetchData() {
    let loader = IExhibitsLoader()
    
    loader.getExhibitList { [weak self] exhibits in
      self?.exhibits = exhibits
      self?.tableView.reloadData()
      self?.updateUI()
    }
  }
  
  private func configureTableView() {
    tableView.register(cellClass: ExhibitTableViewCell.self)
    tableView.estimatedRowHeight = 150.0
    tableView.rowHeight = 150
    noContentView.isHidden = true
  }
  
  private func updateUI() {
    if exhibits.count > 0 {
      //tableView.isHidden = false
      noContentView.isHidden = true
    } else {
      //tableView.isHidden = true
      noContentView.isHidden = false
    }
  }
  
  // MARK: - UITableViewDataSource
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return exhibits.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: ExhibitTableViewCell.self.reuseID) as! ExhibitTableViewCell
    let exhibit = exhibits[indexPath.row]
    cell.model = exhibit
    return cell
  }
  
}
