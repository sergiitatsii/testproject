import UIKit
import Kingfisher

class ImageCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  var model: String? {
    didSet {
      updateUI()
    }
  }
  
  func updateUI() {
    guard
      let urlString = model,
      let url = URL(string: urlString)
    else { return }
    
    let placeholderImage = UIImage(named: "placeholder")
    activityIndicator.startAnimating()
    imageView
      .kf.setImage(with: url,
                   placeholder: placeholderImage) { [weak self] _, _, _, _ in
                    self?.activityIndicator.stopAnimating()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    imageView.image = nil
  }
  
}
