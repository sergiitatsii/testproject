import UIKit
import Model
import FileExhibitsLoader

class ExhibitTableViewCell: UITableViewCell {
  
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var pageControl: UIPageControl!
  
  var imagesUrls = [String]()
  
  var model: Exhibit? {
    didSet {
      updateUI()
    }
  }
  
  func updateUI() {
    guard let exhibit = model else { return }
    titleLabel.text = exhibit.title
    imagesUrls = exhibit.images
    pageControl.numberOfPages = imagesUrls.count
    collectionView.reloadData()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    titleLabel.text = nil
  }
  
}

extension ExhibitTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return imagesUrls.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
    cell.model = imagesUrls[indexPath.row]
    return cell
  }
  
}

// MARK: UICollectionViewDelegateFlowLayout
extension ExhibitTableViewCell: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize
  {
    let screenWidth = UIScreen.main.bounds.size.width
    //let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    return CGSize(width: screenWidth, height: 149)
  }
  
}

// MARK: UIScrollViewDelegate
extension ExhibitTableViewCell: UIScrollViewDelegate {
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    if let indexPath = collectionView?.indexPathsForVisibleItems.first {
      pageControl.currentPage = indexPath.row
    }
  }
}

