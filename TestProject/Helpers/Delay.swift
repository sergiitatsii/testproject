import Foundation

func delay(_ delay: TimeInterval, _ closure: @escaping () -> ()) {
  DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: closure)
}
